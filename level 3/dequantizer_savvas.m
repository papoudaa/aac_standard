function q = dequantizer_savvas(s)

n = max(size(s));
q = zeros(n, 1);

for i = 1:n
    switch s(i)
        case -8
            q(i) = -0.75;
        case -7
            q(i) = -0.65;
        case -6
            q(i) = -0.55;    
        case -5
            q(i) = -0.45;
        case -4
            q(i) = -0.35;
        case -3
            q(i) = -0.25;
        case -2
            q(i) = -0.15;
        case -1
            q(i) = -0.05;
        case 0
            q(i) = 0.05;
        case 1
            q(i) = 0.15;
        case 2
            q(i) = 0.25;
        case 3
            q(i) = 0.35;
        case 4
            q(i) = 0.45;
        case 5
            q(i) = 0.55;
        case 6
            q(i) = 0.65;
        otherwise
            q(i) = 0.75;
    end
end