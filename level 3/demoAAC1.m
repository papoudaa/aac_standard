function SNR = demoAAC1(fNameIn, fNameOut)

% load data
[y,~] = audioread(fNameIn);
% set input size to be a multiple of 2048
y = y(1:floor(size(y,1)/2048)*2048,:);
% add 1024 zeros in the start and end 
y = [zeros(1024,2) ; y ; zeros(1024,2)];

% compute number of frames
NumberOfFrames = (size(y,1) -1024)/ 1024;

%% AA Coder
AACSeq1 = AACoder1(fNameIn);

%% iAA Coder
x = iAACoder1(AACSeq1, fNameOut);

error1 = y(1024:282624,1) - x(1024:282624,1);
error2 = y(1024:282624,2) - x(1024:282624,2);

%% SNR 
SNR_left = snr(y(1024:282624,1),error1);
SNR_right = snr(y(1024:282624,2),error2);

fprintf('Left Channel SNR: %f\n',SNR_left);
fprintf('Right Channel SNR: %f\n',SNR_right);
 
plot(error2);

SNR = SNR_left;


end