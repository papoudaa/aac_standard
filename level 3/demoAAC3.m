function [SNR, datarate, compression] = demoAAC3(fNameIn, fNameOut, frameAACoded)

 % load data
[y,Fs] = audioread(fNameIn);
% set input size to be a multiple of 2048
y = y(1:floor(size(y,1)/2048)*2048,:);
% add 1024 zeros in the start and end 
y = [zeros(1024,2) ; y ; zeros(1024,2)];

clear global
clc

tic
%% AA Coder
AACSeq3 = AACoder3(fNameIn);

%% iAA Coder
x = iAACoder3(AACSeq3, fNameOut);
toc

error1 = y(1024:282624,1) - x(1024:282624,1);
error2 = y(1024:282624,2) - x(1024:282624,2);

%% SNR 
SNR_left = snr(y(1024:282624,1),error1);
SNR_right = snr(y(1024:282624,2),error2);

fprintf('Left Channel SNR: %f\n',SNR_left);
fprintf('Right Channel SNR: %f\n',SNR_right);
 
plot(error2);

SNR = SNR_left;

%% Compression
bits = seq2bits( AACSeq3 );

secs = length(y) / Fs;
dd = dir( fNameIn );

datarate_original = dd.bytes * 8 / secs ;
datarate = bits / secs;

compression = datarate / datarate_original;

end