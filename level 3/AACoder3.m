function AACSeq3 = AACoder3(fNameIn, fnameAACoded)
    
    global huffLut; 
    huffLut = loadLUT();

    AACSeq3 = AACoder2(fNameIn);

    for (i=3:length(AACSeq3))
        
        
        
       AACSeq3(i).chl.SMR = psycho(AACSeq3(i).chl.frameT, AACSeq3(i).frameType, AACSeq3(i-1).chl.frameT, AACSeq3(i-2).chl.frameT);
       AACSeq3(i).chr.SMR = psycho(AACSeq3(i).chr.frameT, AACSeq3(i).frameType, AACSeq3(i-1).chr.frameT, AACSeq3(i-2).chr.frameT);
       [AACSeq3(i).chl.S, AACSeq3(i).chl.sfc, AACSeq3(i).chl.G] = AACquantizer(AACSeq3(i).chl.frameF, AACSeq3(i).frameType, AACSeq3(i).chl.SMR);
       [AACSeq3(i).chr.S, AACSeq3(i).chr.sfc, AACSeq3(i).chr.G] = AACquantizer(AACSeq3(i).chr.frameF, AACSeq3(i).frameType, AACSeq3(i).chr.SMR);
       
       
       %% Huffman
       
       %% mdcts
       [AACSeq3(i).chl.stream, AACSeq3(i).chl.codebook] = encodeHuff(AACSeq3(i).chl.S , huffLut);
       [AACSeq3(i).chr.stream, AACSeq3(i).chr.codebook] = encodeHuff(AACSeq3(i).chr.S , huffLut);
       
      
       if ( strcmp( AACSeq3(i).frameType, 'ESH') )
           
           sfc_left = AACSeq3(i).chl.sfc(2:end,:);
           sfc_right = AACSeq3(i).chr.sfc(2:end,:);
           
           AACSeq3(i).chl.sfc = strings(8,1);
           AACSeq3(i).chr.sfc = strings(8,1);
           
           for (k=1:8)
               
               %% sfcs
               AACSeq3(i).chl.sfc(k) = encodeHuff(sfc_left(:,k) , huffLut, 12);
               AACSeq3(i).chr.sfc(k) = encodeHuff(sfc_right(:,k) , huffLut, 12);
               
           end           
           
       else
           
           sfc_left = AACSeq3(i).chl.sfc(2:end);
           sfc_right = AACSeq3(i).chr.sfc(2:end);
           
           %% sfcs
           AACSeq3(i).chl.sfc = encodeHuff(sfc_left , huffLut, 12);
           AACSeq3(i).chr.sfc = encodeHuff(sfc_right , huffLut, 12);
           
       end
       
    end
     
    for (i=1:2)
        
        AACSeq3(i).chl.SMR = AACSeq3(3).chl.SMR;
        AACSeq3(i).chr.SMR = AACSeq3(3).chr.SMR;
       [AACSeq3(i).chl.S, AACSeq3(i).chl.sfc, AACSeq3(i).chl.G] = AACquantizer(AACSeq3(i).chl.frameF, AACSeq3(i).frameType, AACSeq3(i).chl.SMR);
       [AACSeq3(i).chr.S, AACSeq3(i).chr.sfc, AACSeq3(i).chr.G] = AACquantizer(AACSeq3(i).chr.frameF, AACSeq3(i).frameType, AACSeq3(i).chr.SMR);
      
       
       %% Huffman
       
       %% mdcts
       [AACSeq3(i).chl.stream, AACSeq3(i).chl.codebook] = encodeHuff(AACSeq3(i).chl.S , huffLut);
       [AACSeq3(i).chr.stream, AACSeq3(i).chr.codebook] = encodeHuff(AACSeq3(i).chr.S , huffLut);
       
       %% sfcs
       sfc_left = AACSeq3(i).chl.sfc(2:end);
       sfc_right = AACSeq3(i).chr.sfc(2:end);
       
       AACSeq3(i).chl.sfc = encodeHuff(sfc_left , huffLut, 12);
       AACSeq3(i).chr.sfc = encodeHuff(sfc_right , huffLut, 12);
    
    end
    
end
