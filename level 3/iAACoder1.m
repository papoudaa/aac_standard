function x = iAACoder1(AACSeq1, fNameOut)
    
    

    [rows, columns] = size(AACSeq1);
    if(rows > columns)
        NumberOfFrames = rows;
    else
        NumberOfFrames = columns;
    end
    
    temp = ifilterbank([AACSeq1(1).chl.frameF AACSeq1(1).chr.frameF], AACSeq1(1).frameType, AACSeq1(1).winType);
    xleft(:,1) = temp(:,1);
    xright(:,1) = temp(:,2);
    
    for i= 2 : NumberOfFrames
    
    % Inverse Filterbank
    temp = ifilterbank([AACSeq1(i).chl.frameF AACSeq1(i).chr.frameF], AACSeq1(i).frameType, AACSeq1(i).winType);
    xleft(:,i) = temp(:,1);
    xright(:,i) = temp(:,2);
    
    end
    
    
    
    
    for i=2:NumberOfFrames-1

        outputLeft(1:1024,i) = xleft(1025:2048,i-1) + xleft(1:1024,i);
        outputRight(1:1024,i) = xright(1025:2048,i-1) + xright(1:1024,i);

        outputLeft(1025:2048,i) = xleft(1:1024,i+1) + xleft(1025:2048,i);
        outputRight(1025:2048,i) = xright(1:1024,i+1) + xright(1025:2048,i);

    end

    for i = 1:NumberOfFrames-1
       out(1+1024*(i-1):1024*i,1) =  outputLeft(1:1024,i);
       out(1+1024*(i-1):1024*i,2) =  outputRight(1:1024,i);
    end
    
    x = out;

    audiowrite(fNameOut, x, 48000);

end
