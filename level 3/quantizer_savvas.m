function s = quantizer_savvas(q)

n = max(size(q));
s = zeros(n, 1);

for i = 1:n
    
    if q(i) < -0.7
            s(i) = -8;
    elseif q(i) < -0.6
            s(i) = -7;
    elseif q(i) < -0.5
        s(i) = -6;    
    elseif q(i) < -0.4
        s(i) = -5;
    elseif q(i) < -0.3
        s(i) = -4;
    elseif q(i) < -0.2
        s(i) = -3;
    elseif q(i) < -0.1
        s(i) = -2;
    elseif q(i) < 0
        s(i) = -1;
    elseif q(i) < 0.1
        s(i) = 0;
    elseif q(i) < 0.2
        s(i) = 1;
    elseif q(i) < 0.3
        s(i) = 2;
    elseif q(i) < 0.4
        s(i) = 3;
    elseif q(i) < 0.5
        s(i) = 4;
    elseif q(i) < 0.6
        s(i) = 5;
    elseif q(i) < 0.7
        s(i) = 6;
    else
        s(i) = 7;
    end
    
end