function frameFout = iTNS(frameFin, frameType, TNScoeffs)

if ( strcmp(frameType, 'ESH') )
    
    frameFout = zeros(128,8);
    for j = 1 : 8
        
        tns_hat = dequantizer_savvas( TNScoeffs(:,j) );
        
        num = 1;
        denom = [1; -tns_hat];
        if ( ~isstable( num, denom ) )
           
            r = roots( denom );
            in = find( abs( r ) > 0.98 );

            r( in ) = r( in ) ./ ( abs( r( in ) ) + 0.15 );
            denom = poly( r );
            
        end
        
        assert( isstable( num, denom ) );
        frameFout(:,j) = filter( num, denom, frameFin(:,j) );
        
    end
    
else
    
    tns_hat = dequantizer_savvas( TNScoeffs );
        
    num = 1;
    denom = [1; -tns_hat];
    if ( ~isstable( num, denom ) )

        r = roots( denom );
        in = find( abs( r ) > 0.98 );
        
        r( in ) = r( in ) ./ ( abs( r( in ) ) + 0.15 );
        denom = poly( r );

    end

    assert( isstable( num, denom ) );
    frameFout = filter( num, denom, frameFin );
    
end

end
