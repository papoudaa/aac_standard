function x = iAACoder3(AACSeq3, fNameOut)

    global huffLut;

    for ii=1:length(AACSeq3)
        
        Sl = decodeHuff(AACSeq3(ii).chl.stream, AACSeq3(ii).chl.codebook, huffLut);
        Sr = decodeHuff(AACSeq3(ii).chr.stream, AACSeq3(ii).chr.codebook, huffLut);
%         Sl = AACSeq3(ii).chl.S;
%         Sr = AACSeq3(ii).chr.S;

        
        
        if ( strcmp( AACSeq3(ii).frameType, 'ESH') )
            
            % decode sfcs
            sfc_decoded_left = zeros( 42, 8 );
            sfc_decoded_right = zeros( 42, 8 );
            for j = 1 : 8
               
                % left
                sfc_decoded_left( 1, j ) = AACSeq3(ii).chl.G( j );
                sfc_decoded_left( 2:end, j ) = decodeHuff( char(AACSeq3(ii).chl.sfc( j )), 12, huffLut );
                
                % right
                sfc_decoded_right( 1, j ) = AACSeq3(ii).chr.G( j );
                sfc_decoded_right( 2:end, j ) = decodeHuff( char(AACSeq3(ii).chr.sfc( j )), 12, huffLut );
                
            end
            
            AACSeq3(ii).chl.sfc = sfc_decoded_left;
            AACSeq3(ii).chr.sfc = sfc_decoded_right;
            
            AACSeq3(ii).chl.frameF = iAACquantizer(Sl, AACSeq3(ii).chl.sfc, AACSeq3(ii).chl.G, AACSeq3(ii).frameType);
            AACSeq3(ii).chr.frameF = iAACquantizer(Sr, AACSeq3(ii).chr.sfc, AACSeq3(ii).chr.G, AACSeq3(ii).frameType);
            
        else
            
            % decode sfcs
            AACSeq3(ii).chl.sfc = [AACSeq3(ii).chl.G decodeHuff( AACSeq3(ii).chl.sfc, 12, huffLut )];
            AACSeq3(ii).chr.sfc = [AACSeq3(ii).chr.G decodeHuff( AACSeq3(ii).chr.sfc, 12, huffLut )];
            
            AACSeq3(ii).chl.frameF = iAACquantizer(Sl', AACSeq3(ii).chl.sfc, AACSeq3(ii).chl.G, AACSeq3(ii).frameType);
            AACSeq3(ii).chr.frameF = iAACquantizer(Sr', AACSeq3(ii).chr.sfc, AACSeq3(ii).chr.G, AACSeq3(ii).frameType); 
            
        end

    end
    
    x = iAACoder2( AACSeq3, fNameOut );
    
    
end
