function [S, sfc, G] = AACquantizer(frameF, frameType, SMR)

global tableL
global tableS


%% constants
MQ = 8191;
MagicNumber = 0.4054;

if (strcmp(frameType,'ESH'))
    
    nbands = max(size(tableS));
    a = zeros(nbands,8);
    table = tableS;
    Xq = zeros(1024,8);
    flag2=0;
    
    for i=1:8 
        
        flag = zeros(nbands,1);
        
        %% energy of mdct coefficients
        for j=1:nbands

            wlow = table(j,2)+1;
            whigh = table(j,3)+1;
            P(j,i) = sumsqr(frameF(wlow:whigh,i));

        end
        
        T(:,i) = P(:,i) ./ SMR(:,i); 

        a(:,i) = (16/3)*log2(((max(frameF(:,i)))^(3/4))/MQ);
        
         for k=1:nbands
             wlow = table(k,2)+1;
             whigh = table(k,3)+1;

             %% quantization
             S(wlow:whigh,i) = sign(frameF(wlow:whigh,i)) .* floor((abs(frameF(wlow:whigh,i)) * 2^(-0.25*a(k,i))).^(0.75) + MagicNumber);

             %% dequantization
             Xq(wlow:whigh,i) = sign(S(wlow:whigh,i)) .* (abs(S(wlow:whigh,i))).^(4/3) * 2.^(0.25*a(k,i)); 
         
         end  
         
         
         while 1

            for k=1:nbands
                               
                wlow = table(k,2)+1;
                whigh = table(k,3)+1;
                Pe = sumsqr(frameF(wlow:whigh,i)-Xq(wlow:whigh,i));
                
                if ((Pe<T(k,i)) && (flag(k)==0))
                    
                    wlow = table(k,2)+1;
                    whigh = table(k,3)+1;
                
                    a(k,i) = a(k,i) + 1;

                    %% quantization
                    S(wlow:whigh,i) = sign(frameF(wlow:whigh,i)) .* floor((abs(frameF(wlow:whigh,i)) * 2^(-0.25*a(k,i))).^(0.75) + MagicNumber);

                    %% dequantization
                    Xq(wlow:whigh,i) = sign(S(wlow:whigh,i)) .* (abs(S(wlow:whigh,i))).^(4/3) * 2.^(0.25*a(k,i)); 
                
                else
                    if (flag(k)==0)
                        a(k,i) = a(k,i) - 1;

                        %% quantization
                        S(wlow:whigh,i) = sign(frameF(wlow:whigh,i)) .* floor((abs(frameF(wlow:whigh,i)) .* 2^(-0.25*a(k,i))).^(0.75) + MagicNumber);
                            
                        %% dequantization
                        Xq(wlow:whigh,i) = sign(S(wlow:whigh,i)) .* (abs(S(wlow:whigh,i))).^(4/3) * 2.^(0.25*a(k,i)); 
                        
                    end
                    flag(k)=1;
                end
                
                if (isequal(flag, ones(nbands,1)))
                    flag2 =1;
                    break
                end
                
                if ((max(abs(diff(a(:,i)))))>59)
                        flag2=1;
                        break
                end
            
            end

            if (flag2==1)
                break
            end
        
        end

        G(i) = a(1,i); 
        sfc(:,i)= [G(i) ; diff(a(:,i))];


    end
    
    S = S(:);
    
    
else
    
    table = tableL;
    nbands = max(size(table));
    a = zeros(nbands,1);
    
    %% energy of mdct coefficients
    for i=1:nbands
        P(i) = sumsqr(frameF(table(i,2)+1:table(i,3)+1));
    end
    
    T = P' ./ SMR;
    
    %% Scale Factor Gain a
    a(:) = (16/3)*log2(((max(frameF))^(3/4))/MQ);
    flag = zeros(nbands,1);
    flag2=0;
    
    for (i=1:nbands)
        
        wlow = table(i,2)+1;
        whigh = table(i,3)+1;
        
        %% quantization
        S(wlow:whigh) = sign(frameF(wlow:whigh)) .* floor((abs(frameF(wlow:whigh)) * 2^(-0.25*a(i))).^(0.75) + MagicNumber);

        %% dequantization
        Xq = sign(S(wlow:whigh)) .* (abs(S(wlow:whigh))).^(4/3) * 2^(0.25*a(i)); 

        Pe = sumsqr(frameF(wlow:whigh)-Xq');
    end
        
    while 1

        for i=1:nbands
            
             Pe = sumsqr(frameF(wlow:whigh)-Xq');
            
            if ((Pe<T(i)) && (flag(i)==0))
                
                wlow = table(i,2)+1;
                whigh = table(i,3)+1;

                a(i) = a(i) + 1;

                %% quantization
                S(wlow:whigh) = sign(frameF(wlow:whigh)) .* floor((abs(frameF(wlow:whigh)) * 2^(-0.25*a(i))).^(0.75) + MagicNumber);

                %% dequantization
                Xq = sign(S(wlow:whigh)) .* (abs(S(wlow:whigh))).^(4/3) * 2^(0.25*a(i)); 


            else
                
                if (flag(i)==0)
                    a(i) = a(i) - 1;

                    %% quantization
                    S(wlow:whigh) = sign(frameF(wlow:whigh) .* floor((abs(frameF(wlow:whigh)) .* 2^(-0.25*a(i))).^(0.75) + MagicNumber));

                end
                
                flag(i)=1;
            end
            if (isequal(flag, ones(nbands,1)))
                    flag2 =1;
                    break
            end
            if ((max(abs(diff(a))))>59)
                    flag2=1;
                    break
            end
            
        end
        
        if (flag2==1)
            break
        end
        
        
    end
    
   
    G = a(1);
    sfc= [G; diff(a)];
        
end




end
