function [frameFout, TNScoeffs] = TNS(frameFin, frameType)

global tableL tableS
if ( isempty( tableL ) )
    
    temp123 = load('TableB219.mat','B219a','B219b');
    tableL = temp123.B219a;
    tableS = temp123.B219b;
    
end

if(strcmp(frameType, 'ESH'))
    
    Sw = zeros(128,8);
    TNScoeffs = zeros(4,8);
    frameFout = zeros( 128, 8 );
    for j = 1:8

%         count = 1;
%         for i = 1:128
%             
%             for k = tableS(count,2) + 1:(tableS(count,3) + 1)
%                 Sw(i,j) = Sw(i,j) + frameFin(k,j)^2;
%             end
%             Sw(i,j) = sqrt(Sw(i,j));
%             if i == tableS(count,3) + 1
%                 count = count+1;
%             end
%             
%         end

        P = zeros( 42, 1 );
        for b = 1 : 42

            klow = tableS( b, 2 ) + 1;
            khigh = tableS( b, 3 ) + 1;

            % Band Energy
            P( b ) = sumsqr( frameFin( klow : khigh, j ) );

            % Normalizing Coefficients for this Band
            Sw( klow : khigh, j ) = sqrt( P( b ) );

        end
        
        for i= 127:-1:1
            Sw(i,j)=(Sw(i,j)+Sw(i+1,j))/2;
        end
        for i= 2:1:127
            Sw(i,j)=(Sw(i,j)+Sw(i-1,j))/2;
        end
        
        frameFout(:,j) = frameFin(:,j)./Sw(:,j);
%         A = lpc(frameFout(:,j), 4);
%         A = -A( 2:end );

        [c, lags] = xcov(frameFout(:,j),4);
        c = c/c(lags==0);
        r = c(lags>0);
        r1 = c(lags>=0);
        r1 = r1(1:end-1);
        R = toeplitz(r1);
        A = R\r;

        TNScoeffs(:,j) = quantizer_savvas(A);
        tns_hat = dequantizer_savvas( TNScoeffs(:,j) );
        
        num = [1; -tns_hat];
        denom = 1;
        if ( ~isstable( denom, num ) )
           
            num_roots = roots( num );
            in = find( abs( num_roots ) > 0.98 );

            num_roots( in ) = num_roots( in ) ./ ( abs( num_roots( in ) ) + 0.15 );
            num = poly( num_roots );
            
        end
        
        assert( isstable( denom, num ) );
        frameFout(:,j) = filter( num, denom, frameFin(:,j) );
    end
    
else
    
    P = zeros( 69, 1 );
    Sw = zeros( 1024, 1 );
    for b = 1 : 69

        klow = tableL( b, 2 ) + 1;
        khigh = tableL( b, 3 ) + 1;

        % Band Energy
        P( b ) = sumsqr( frameFin( klow : khigh ) );

        % Normalizing Coefficients for this Band
        Sw( klow : khigh ) = sqrt( P( b ) );

    end
    
    for i= 1023:-1:1
        Sw(i)=(Sw(i)+Sw(i+1))/2;
    end

    for i= 2:1:1024
        Sw(i)=(Sw(i)+Sw(i-1))/2;
    end

    frameFout = frameFin./Sw;
    
    % lpc coeffs
%     A = lpc(frameFout, 4);
%     A = -A( 2:end );
    [c, lags] = xcov(frameFout,4);
    c = c/c(lags==0);
    r = c(lags>0);
    r1 = c(lags>=0);
    r1 = r1(1:end-1);
    R = toeplitz(r1);
    A = R\r;

    TNScoeffs(:,1) = quantizer_savvas(A);
    tns_hat = dequantizer_savvas( TNScoeffs );

    num = [1; -tns_hat];
    denom = 1;
    if ( ~isstable( denom, num ) )

        num_roots = roots( num );
        in = find( abs( num_roots ) > 0.98 );
        
        num_roots( in ) = num_roots( in ) ./ ( abs( num_roots( in ) ) + 0.15 );
        num = poly( num_roots );

    end

    assert( isstable( denom, num ) );
    frameFout = filter( num, denom, frameFin );
end

end
