
function SMR = psycho(frameT, frameType, frameTprev1, frameTprev2)

global tableL tableS
global sfOls sfEsh

%% Constants
n1 = 0:2047;
n2 = 0:255;

if (frameType == 'ESH')
    
    if ( isempty( tableS ) )
        temp = load('TableB219.mat');
        tableS = temp.B219b;
    end
    
    
    %% step 1
    %% computation of spreading function table
    if (isempty(sfEsh))
        for i=1:(size(tableS,1))
            for j=1:(size(tableS,1))
                sfEsh(i,j) = spreadingfun(i,j,'ESH');
            end
        end
    end    
    
    %% step 2 Apply Hann Window
    subFramesT = buffer([frameT(449:(449+1151))],256,128,'nodelay');
    subFramesTprev1 = buffer([frameTprev1(449:(449+1151))],256,128,'nodelay');
    
    for i=1:8
        
        subFramesT(:,i) = subFramesT(:,i) .* (0.5 - 0.5*cos(pi*(n2+0.5)/256))';
        subFramesTprev1(:,i) = subFramesTprev1(:,i) .* (0.5 - 0.5*cos(pi*(n2+0.5)/256))';
        
        %% FFT
        subFramesTFourier(:,i) = fft(subFramesT(:,i));
        subFramesTprev1Fourier(:,i) = fft(subFramesTprev1(:,i));
        
        %% magnitude and phase of fft
        r(:,i) = abs(subFramesTFourier(:,i));
        rprev1(:,i) = abs(subFramesTprev1Fourier(:,i));
        
        f(:,i) = angle(subFramesTFourier(:,i));
        fprev1(:,i) = angle(subFramesTprev1Fourier(:,i));
    end
    
    %% step 3
    %% predicitons
    rPred(:,1) = 2*rprev1(:,8) - rprev1(:,7);
    fPred(:,1) = 2*fprev1(:,8) - fprev1(:,7);
    rPred(:,2) = 2*r(:,1) - rprev1(:,8);
    fPred(:,2) = 2*f(:,1) - fprev1(:,8);
    
    for (i=3:8)
        rPred(:,i) = 2*r(:,i-1) - r(:,i-2);
        fPred(:,i) = 2*f(:,i-1) - f(:,i-2);
    end
    
    
    for (i=1:8)
        
        %% step 4
        %% predictability
        cw(:,i) = sqrt((r(:,i).*cos(f(:,i))-rPred(:,i).*cos(fPred(:,i))).^2 + (r(:,i).*sin(f(:,i))-rPred(:,i).*sin(fPred(:,i))).^2)./(r(:,i)+abs(rPred(:,i)));
        
        %% step 5
        for (j=1:42)
             e(j,i) = sumsqr(r(tableS(j,2)+1:tableS(j,3)+1,i));
             c(j,i) = sum(cw(tableS(j,2)+1:tableS(j,3)+1,i).*(r(tableS(j,2)+1:tableS(j,3)+1,i)).^2);
        end 
        
        %% step 6
        ecb(:,i) = sfEsh' * e(:,i);
        ct(:,i) = sfEsh' * c(:,i);

        %% normalization
        cb(:,i) = ct(:,i) ./ ecb(:,i);
        en(:,i) = ecb(:,i) ./ sum(sfEsh)';
        
        %% step 7
        %% tonality index
        tb(:,i) = -0.299 -0.43 * log(cb(:,i));
        
        %% step 8
        %% Compute SNR of the bands
        SNR(:,i) = 18*tb(:,i) + 6*(1-tb(:,i));
        
        %% step 9
        bc(:,i) = 10.^(-SNR(:,i)/10);
        
        %% step 10
        nb(:,i) = en(:,i) .* bc(:,i);
        
        %% step 11 
        %% level of noise

        qthr = eps() * (256/2) * (10.^(tableS(:,6)/10));

        npart(:,i) = max(nb(:,i),qthr);

        %% step 12
        %% Signal to Mask Ratio
        SMR(:,i) = e(:,i) ./ npart(:,i);
        
        
    end
    
else
    
    if ( isempty( tableL ) )
        temp = load('TableB219.mat');
        tableL = temp.B219a;
    end
    
    %% step 1
    %% Computation of spreading function table    
    if (isempty(sfOls))
        for i=1:(size(tableL,1))
            for j=1:(size(tableL,1))
                sfOls(i,j) = spreadingfun(i,j,'OLS');
            end
        end
    end    
    
    %% step 2
    %% Apply Hann Window
    frameT = frameT .* (0.5 - 0.5*cos(pi*(n1+0.5)/2048))';
    frameTprev1 = frameTprev1 .* (0.5 - 0.5*cos(pi*(n1+0.5)/2048))';
    frameTprev2 = frameTprev2 .* (0.5 - 0.5*cos(pi*(n1+0.5)/2048))';
    
    %% FFT 
    frameTFourier = fft(frameT);
    frameTprev1Fourier = fft(frameTprev1);
    frameTprev2Fourier = fft(frameTprev2);
    
    %% magnitude and phase of fft
    r = abs(frameTFourier);
    rprev1 = abs(frameTprev1Fourier);
    rprev2 = abs(frameTprev2Fourier);
    
    f = angle(frameTFourier);
    fprev1 = angle(frameTprev1Fourier);
    fprev2 = angle(frameTprev2Fourier);
    
    %% step 3
    %% predictions
    rPred = 2 * rprev1 - rprev2;
    fPred = 2 * fprev1 - fprev2;
     
    %% step 4
    %% predictability
    cw = sqrt((r.*cos(f)-rPred.*cos(fPred)).^2 + (r.*sin(f)-rPred.*sin(fPred)).^2) ./ (r+abs(rPred));
    
    %% step 5
    for (i=1:69)
         e(i) = sumsqr(r(tableL(i,2)+1:tableL(i,3)+1));
         c(i) = sum(cw(tableL(i,2)+1:tableL(i,3)+1).*(r(tableL(i,2)+1:tableL(i,3)+1)).^2);
    end 
    
    %% step 6
    ecb = sfOls' * e';
    ct = sfOls' * c';
    
    %% normalization
    cb = ct ./ ecb;
    en = ecb ./ sum(sfOls)';
    
    %% step 7
    %% tonality index
    tb = -0.299 -0.43 * log(cb);
    
    %% step 8
    %% Compute SNR of the bands
    SNR = 18*tb + 6*(1-tb);
    
    %% step 9
    bc = 10.^(-SNR/10);
    
    %% step 10
    nb = en .* bc;
    
    %% step 11 
    %% level of noise
    
    qthr = eps() * (2048/2) * (10.^(tableL(:,6)/10));
    npart = max(nb,qthr);
    
    %% step 12
    %% Signal to Mask Ratio
    SMR = e' ./ npart;
   
end

end