function frameF = iAACquantizer( S, sfc, G, frameType )

    global tableL
    global tableS
  
    if (frameType == 'ESH')
        
        S = buffer(S,128);
        table = tableS;
        nbands = size(table,1);
       
        for i=1:8    
            a(:,i) = cumsum(sfc(:,i));

            for k=1:nbands

                    wlow = table(k,2)+1;
                    whigh = table(k,3)+1;

                    %% dequantization
                    frameF(wlow:whigh, i) = sign(S(wlow:whigh,i)) .* (abs(S(wlow:whigh,i))).^(4/3) * 2^(0.25*a(k,i)); 
            end
            
        end


    else

        a = cumsum(sfc);
        table = tableL;
        nbands = size( table, 1 );
        
        %% dequantization
        for i = 1:size( table, 1 )
            
            wlow = table(i,2)+1;
            whigh = table(i,3)+1;
            
            frameF(wlow:whigh) = sign(S(wlow:whigh)) .* (abs(S(wlow:whigh))).^(4/3) * 2^(0.25*a(i));  
            
        end
        
        frameF = frameF';
    end

end

