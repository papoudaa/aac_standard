function bits = seq2bits(seq)

    bits = 0;
    for i = 1 : length( seq )
       
        is_esh = strcmp( seq( i ).frameType, 'ESH' );
        
        % left channel
        bits = bits + length( seq( i ).chl.stream );
        if ( is_esh )
           
            for j = 1 : 8
               
                bits = bits + length( seq( i ).chl.sfc( j ) );
                bits = bits + 32;   % G is 32 bits ( single precision )
                bits = bits + 16;   % 4 * 4 bits for Quantized TNSCoeffs
                
            end
           
        else
            
            bits = bits + length( seq( i ).chl.sfc );
            bits = bits + 32;   % G is 32 bits ( single precision )
            bits = bits + 16;   % 4 * 4 bits for Quantized TNSCoeffs
            
        end
        
        % right channel
        bits = bits + length( seq( i ).chr.stream );
        if ( is_esh )
           
            for j = 1 : 8
               
                bits = bits + length( seq( i ).chr.sfc( j ) );
                bits = bits + 32;   % G is 32 bits ( single precision )
                bits = bits + 16;   % 4 * 4 bits for Quantized TNSCoeffs
                
            end
           
        else
            
            bits = bits + length( seq( i ).chr.sfc );
            bits = bits + 32;   % G is 32 bits ( single precision )
            bits = bits + 16;   % 4 * 4 bits for Quantized TNSCoeffs
            
        end
        
    end

end

