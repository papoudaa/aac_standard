function  frameType = SSC(frameT,nextFrameT,prevFrameType)

if (prevFrameType == 'LPS')
    frameType = 'OLS';
    return;
elseif (prevFrameType == 'LSS')
    frameType = 'ESH';
    return;
end    

% filter coefficients
a = [0.7548 -0.7548];
b = [1 -0.5095];

% filter the i+1 frame 
y1 = filter(a,b,nextFrameT(:,1));
y2 = filter(a,b,nextFrameT(:,2));

% compute the square of sl with l = 1,2..8
for i=0:7
    s1(i+1) = sumsqr(y1((576+128*i+1):(576+128*(i+1))));
    s2(i+1) = sumsqr(y2((576+128*i+1):(576+128*(i+1))));
end

% compute the attack values
for i=1:8
    d1(i) = s1(i) / ((1/i)*(sum(s1(1:(i-1)))));
    d2(i) = s2(i) / ((1/i)*(sum(s2(1:(i-1)))));
end

% type of each channel
type1 = 'OLS';
for i=2:8
    if (s1(i)>0.001 && d1(i)>10)
        type1 = 'ESH';
        break;
    end
end

type2 = 'OLS';
for i=2:8
    if (s2(i)>0.001 && d2(i)>10)
        type2 = 'ESH';
        break;
    end
end

if ((type1=='OLS') & (type2 == 'OLS'))
    nextFrameType = 'OLS';
elseif ((type1 == 'OLS') & (type2 == 'ESH'))
    nextFrameType = 'ESH';
elseif ((type1 == 'ESH') & (type2 == 'OLS'))   
    nextFrameType = 'ESH';
elseif (type1 == 'ESH' & type2 =='ESH')
    nextFrameType = 'ESH';
end

if (prevFrameType == 'OLS')
    if (nextFrameType == 'OLS')
        frameType = 'OLS';
    elseif (nextFrameType=='ESH')
        frameType = 'LSS';
    end
else
    if (nextFrameType=='ESH')
        frameType = 'ESH';
    elseif (nextFrameType == 'OLS')
        frameType = 'LPS';
    end
end

return;