function AACSeq1 = AACoder1(fNameIn)
%% AA Coder Level 1

N = 2048;
WINTYPE = 'SIN';

% load data
[y,~] = audioread(fNameIn);

% set input size to be a multiple of 2048
y = y(1:floor(size(y,1)/2048)*2048,:);

% add 1024 zeros in the start and end 
y = [zeros(1024,2) ; y ; zeros(1024,2)];

% compute number of frames
NumberOfFrames = (size(y,1) -1024)/ 1024;

% left channel
leftCh = buffer(y(:,1),N,N/2,'nodelay');

% right channel
rightCh = buffer(y(:,2),N,N/2,'nodelay');

%% Coder

% First Frame
AACSeq1(1).frameType = 'OLS';
AACSeq1(1).winType = WINTYPE;
AACSeq1(1).chl.frameT = leftCh(:,1);
AACSeq1(1).chr.frameT = rightCh(:,1);
temp = filterbank([leftCh(:,1)  rightCh(:,1)], AACSeq1(1).frameType, AACSeq1(1).winType);

if (strcmp(AACSeq1(1).frameType,'ESH'))
    AACSeq1(1).chl.frameF = temp(:,:,1);
    AACSeq1(1).chr.frameF = temp(:,:,2);
else
    
    AACSeq1(1).chl.frameF = temp(:,1);
    AACSeq1(1).chr.frameF = temp(:,2);
    
end 
for i= 2 : NumberOfFrames-1
     
    % Sequence segmentation
    AACSeq1(i).frameType = SSC([leftCh(:,i)  rightCh(:,i)],[leftCh(:,i+1)  rightCh(:,i+1)],AACSeq1(i-1).frameType);
    
    % Filterbank
    AACSeq1(i).winType = WINTYPE;
    temp = filterbank([leftCh(:,i)  rightCh(:,i)], AACSeq1(i).frameType, AACSeq1(i).winType);
    
    if (strcmp(AACSeq1(i).frameType,'ESH'))
        
        AACSeq1(i).chl.frameF = temp(:,:,1);
        AACSeq1(i).chr.frameF = temp(:,:,2);
        
    else
    
        AACSeq1(i).chl.frameF = temp(:,1);
        AACSeq1(i).chr.frameF = temp(:,2);
        
    end 
    AACSeq1(i).chl.frameT = leftCh(:,i);
    AACSeq1(i).chr.frameT = rightCh(:,i);
        
end


% Last Frame

AACSeq1(NumberOfFrames).frameType = 'OLS';
AACSeq1(NumberOfFrames).winType = WINTYPE;
temp = filterbank([leftCh(:,NumberOfFrames)  rightCh(:,NumberOfFrames)], AACSeq1(NumberOfFrames).frameType, AACSeq1(NumberOfFrames).winType);

if (strcmp(AACSeq1(NumberOfFrames).frameType,'ESH'))
    AACSeq1(NumberOfFrames).chl.frameF = temp(:,:,1);
    AACSeq1(NumberOfFrames).chr.frameF = temp(:,:,2);
else
    AACSeq1(NumberOfFrames).chl.frameF = temp(:,1);
    AACSeq1(NumberOfFrames).chr.frameF = temp(:,2); 
end 

AACSeq1(NumberOfFrames).chl.frameT = leftCh(:,NumberOfFrames);
AACSeq1(NumberOfFrames).chr.frameT = rightCh(:,NumberOfFrames);


end
