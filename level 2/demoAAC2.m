function SNR = demoAAC2(fNameIn, fNameOut)

% load data
[y,~] = audioread(fNameIn);
% set input size to be a multiple of 2048
y = y(1:floor(size(y,1)/2048)*2048,:);
% add 1024 zeros in the start and end 
y = [zeros(1024,2) ; y ; zeros(1024,2)];

tic
%% AA Coder
AACSeq2 = AACoder2(fNameIn);

%% iAA Coder
x = iAACoder2(AACSeq2, fNameOut);
toc

error1 = y(1024:282624,1) - x(1024:282624,1);
error2 = y(1024:282624,2) - x(1024:282624,2);

%% SNR 
SNR_left = snr(y(1024:282624,1),error1);
SNR_right = snr(y(1024:282624,2),error2);

fprintf('Left Channel SNR: %f\n',SNR_left);
fprintf('Right Channel SNR: %f\n',SNR_right);
 
plot(error1);
% xlabel('samples');
% ylabel('error');

SNR = SNR_left;

end