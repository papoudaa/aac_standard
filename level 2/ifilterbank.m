function frameT = ifilterbank(frameF, frameType, winType)

N = 2048;
n1 = 0:(N-1);
n2 = 0:(N/8-1);
y = zeros(N,2);
frameT = zeros(N,2);

WKBD_N = zeros(N,1);
WKBD_N8 = zeros(N/8,1);
WSIN_N = zeros(N,1);
WSIN_N8 = zeros(N/8,1);

%% Inverse Modified Discrete Cosine Transform (iMDCT)

if (strcmp(frameType,'ESH'))
%     
%     frameF = reshape(frameF,[128 8 2]);
%        

    frameF_left =[zeros(128,1) frameF(:,1:8) zeros(128,1)];
    frameF_right =[zeros(128,1) frameF(:,9:16) zeros(128,1)];
    
    for i=1:10
        yleft(:,i) = imdct4(frameF_left(1:128,i));
        yright(:,i) = imdct4(frameF_right(1:128,i));
    end
    
else 
    
    y = imdct4(frameF);

end

if (winType=='KBD')
  
  
    % Apply the Window
    if (strcmp(frameType,'OLS'))
        
        sum1 = 0;
        sum2 = 0;

        %% WKBD for OLS with 2048 points
        vector = kaiser(N/2+1,6); 
        sum1 = sum(vector);
        sum1 = sqrt(sum1);
        for i=1:1024

            sum2 = sum2 + vector(i);
            WKBD_N(i) = sqrt(sum2) / sum1;
            WKBD_N(2049-i) =  WKBD_N(i);

        end


        frameT(1:N,1) = y(1:N,1) .* WKBD_N(1:N);
        frameT(1:N,2) = y(1:N,2) .* WKBD_N(1:N);

        
    elseif (strcmp(frameType,'LSS'))
        
        sum1 = 0;
        sum2 = 0;

        %% WKBD for OLS with 2048 points
        vector = kaiser(N/2+1,6); 
        sum1 = sum(vector);
        sum1 = sqrt(sum1);
        for i=1:1024

            sum2 = sum2 + vector(i);
            WKBD_N(i) = sqrt(sum2) / sum1;
            WKBD_N(2049-i) =  WKBD_N(i);

        end

        sum1 = 0;
        sum2 = 0;

        %% WKBD with 256 points

        vector = kaiser(129,4); 
        sum1 = sum(vector);
        sum1 = sqrt(sum1);
        for i=1:128
            sum2 = sum2 + vector(i);
            WKBD_N8(i) = sqrt(sum2) / sum1;
            WKBD_N8(257-i) =  WKBD_N8(i);
        end

        frameT(1:N/2,1) = y(1:N/2,1) .* WKBD_N(1:N/2);
        frameT(1:N/2,2) = y(1:N/2,2) .* WKBD_N(1:N/2);
       
        
        frameT((N/2+1):(N/2+448),1) = y((N/2+1):(N/2+448),1);
        frameT((N/2+1):(N/2+448),2) = y((N/2+1):(N/2+448),2);
        
        
        frameT((N/2+448+1):(N/2+127+448+1),1) = y((N/2+448+1):(N/2+127+448+1),1) .* WKBD_N8((N/16+1):(N/8));
        frameT((N/2+448+1):(N/2+127+448+1),2) = y((N/2+448+1):(N/2+127+448+1),2) .* WKBD_N8((N/16+1):(N/8)); 
      

        
    elseif (strcmp(frameType,'LPS'))
        
        sum1 = 0;
        sum2 = 0;

        %% WKBD for OLS with 2048 points
        vector = kaiser(N/2+1,6); 
        sum1 = sum(vector);
        sum1 = sqrt(sum1);
        for i=1:1024

            sum2 = sum2 + vector(i);
            WKBD_N(i) = sqrt(sum2) / sum1;
            WKBD_N(2049-i) =  WKBD_N(i);

        end

        %% WKBD with 256 points
        
        sum1=0;
        sum2=0;
        
        vector = kaiser(129,4); 
        sum1 = sum(vector);
        sum1 = sqrt(sum1);
        for i=1:128
            sum2 = sum2 + vector(i);
            WKBD_N8(i) = sqrt(sum2) / sum1;
            WKBD_N8(257-i) =  WKBD_N8(i);
        end
        
        frameT((449):(449+127),1) = y((449):(449+127),1) .* WKBD_N8(1:N/16) ;
        frameT((449):(449+127),2) = y((449):(449+127),2) .* WKBD_N8(1:N/16) ;
       
        frameT((449+128):(449+128+447),1) = y((449+128):(449+128+447),1) ;
        frameT((449+128):(449+128+447),2) = y((449+128):(449+128+447),2) ;
        
        frameT(N/2+1:N,1) = y(N/2+1:N,1) .* WKBD_N(N/2+1:N) ;
        frameT(N/2+1:N,2) = y(N/2+1:N,2) .* WKBD_N(N/2+1:N) ;

        
    else
        
        %% WKBD with 256 points
        sum1=0;
        sum2=0;
        
         
        vector = kaiser(129,4); 
        sum1 = sum(vector);
        sum1 = sqrt(sum1);
        for i=1:128
            sum2 = sum2 + vector(i);
            WKBD_N8(i) = sqrt(sum2) / sum1;
            WKBD_N8(257-i) =  WKBD_N8(i);
        end
        
        for i=1:10
            
            ESH_YL(i,:) = yleft(1:256,i) .* WKBD_N8(1:256);
            ESH_YH(i,:) = yright(1:256,i) .* WKBD_N8(1:256);    
            
        end
        
        ESH_YL(1,129:256) =  ESH_YL(1,129:256) + ESH_YL(2,1:128);
        ESH_YH(1,129:256) =  ESH_YH(1,129:256) + ESH_YH(2,1:128);
        
        for i=2:9
            
           ESH_YL(i,1:128) = ESH_YL(i-1,129:256);
           ESH_YH(i,1:128) = ESH_YH(i-1,129:256);
           
           ESH_YL(i,129:256) =  ESH_YL(i,129:256) + ESH_YL(i+1,1:128);
           ESH_YH(i,129:256) =  ESH_YH(i,129:256) + ESH_YH(i+1,1:128);
           
        end
        
        ESH_YL(10,1:128) = ESH_YL(9,129:256);
        ESH_YH(10,1:128) = ESH_YH(9,129:256);
        
        for i=2:10
            frameT(449+128*(i-2):448+128*(i-2)+128,1) = ESH_YL(i,1:128);
            frameT(449+128*(i-2):448+128*(i-2)+128,2) = ESH_YH(i,1:128);
        end  
        
        
    end
    
else
    
    %% Window Function: sinusoid (SIN)
    

    % Apply the Window
    if (strcmp(frameType,'OLS'))
        
        %% WSIN with 2056 points
        WSIN_N = (sin((pi/2048)*(n1+0.5)))';
   
        frameT(1:N,1) = y(1:N,1) .* WSIN_N(1:N);
        frameT(1:N,2) = y(1:N,2) .* WSIN_N(1:N);
        
    elseif (strcmp(frameType,'LSS'))
        
        %% WSIN with 2056 points
        WSIN_N = (sin((pi/2048)*(n1+0.5)))';
    
        %% WSIN with 256 points
        WSIN_N8 = (sin((pi/256)*(n2+0.5)))';
        
        frameT(1:N/2,1) = y(1:N/2,1) .* WSIN_N(1:N/2);
        frameT(1:N/2,2) = y(1:N/2,2) .* WSIN_N(1:N/2);
       
        
        frameT((N/2+1):(N/2+448),1) = y((N/2+1):(N/2+448),1);
        frameT((N/2+1):(N/2+448),2) = y((N/2+1):(N/2+448),2);
        
        
        frameT((N/2+448+1):(N/2+127+448+1),1) = y((N/2+448+1):(N/2+127+448+1),1) .* WSIN_N8((N/16+1):(N/8));
        frameT((N/2+448+1):(N/2+127+448+1),2) = y((N/2+448+1):(N/2+127+448+1),2) .* WSIN_N8((N/16+1):(N/8));
        
    elseif (strcmp(frameType,'LPS'))
        
        %% WSIN with 2056 points
        WSIN_N = (sin((pi/2048)*(n1+0.5)))';
    
        %% WSIN with 256 points
        WSIN_N8 = (sin((pi/256)*(n2+0.5)))';
    
        frameT((449):(449+127),1) = y((449):(449+127),1) .* WSIN_N8(1:N/16) ;
        frameT((449):(449+127),2) = y((449):(449+127),2) .* WSIN_N8(1:N/16) ;
       
        frameT((449+128):(449+128+447),1) = y((449+128):(449+128+447),1) ;
        frameT((449+128):(449+128+447),2) = y((449+128):(449+128+447),2) ;
        
        frameT(N/2+1:N,1) = y(N/2+1:N,1) .* WSIN_N(N/2+1:N) ;
        frameT(N/2+1:N,2) = y(N/2+1:N,2) .* WSIN_N(N/2+1:N) ;
        
    else
       
    
        %% WSIN with 256 points
        WSIN_N8 = (sin((pi/256)*(n2+0.5)))';
        
        for i=1:10
            
            ESH_YL(i,:) = yleft(1:256,i) .* WSIN_N8(1:256);
            ESH_YH(i,:) = yright(1:256,i) .* WSIN_N8(1:256);    
            
        end
        
        ESH_YL(1,129:256) =  ESH_YL(1,129:256) + ESH_YL(2,1:128);
        ESH_YH(1,129:256) =  ESH_YH(1,129:256) + ESH_YH(2,1:128);
        
        for i=2:9
            
           ESH_YL(i,1:128) = ESH_YL(i-1,129:256);
           ESH_YH(i,1:128) = ESH_YH(i-1,129:256);
           
           ESH_YL(i,129:256) =  ESH_YL(i,129:256) + ESH_YL(i+1,1:128);
           ESH_YH(i,129:256) =  ESH_YH(i,129:256) + ESH_YH(i+1,1:128);
           
        end
        
        ESH_YL(10,1:128) = ESH_YL(9,129:256);
        ESH_YH(10,1:128) = ESH_YH(9,129:256);
        
        for i=2:10
            frameT(449+128*(i-2):448+128*(i-2)+128,1) = ESH_YL(i,1:128);
            frameT(449+128*(i-2):448+128*(i-2)+128,2) = ESH_YH(i,1:128);
        end       
        
    end
    
end


end
