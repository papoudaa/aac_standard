function frameF = filterbank(frameT, frameType, winType)

N = 2048;
n1 = 0 : (N-1) ;
n2 = 0 : (N/8-1) ; 
y = zeros(N,2); 
WKBD_N = zeros(N,1);
WKBD_N8 = zeros(N/8,1);
WSIN_N = zeros(N,1);
WSIN_N8 = zeros(N/8,1);


%% Apply Window function before MDCT
if (winType=='KBD')
        
    % Apply the Window
    if (strcmp(frameType,'OLS'))
        
        sum1 = 0;
        sum2 = 0;

        %% WKBD for OLS with 2048 points
        vector = kaiser(N/2+1,6); 
        sum1 = sum(vector);
        sum1 = sqrt(sum1);
        
        for i=1:(N/2)

            sum2 = sum2 + vector(i);
            WKBD_N(i) = sqrt(sum2) / sum1;
            WKBD_N(2049-i) =  WKBD_N(i);

        end

        
        y(1:N,1) = frameT(1:N,1) .* WKBD_N(1:N);
        y(1:N,2) = frameT(1:N,2) .* WKBD_N(1:N);
       
        
    elseif (strcmp(frameType,'LSS'))
        
        sum1 = 0;
        sum2 = 0;

        %% WKBD for OLS with 2048 points
        vector = kaiser(N/2+1,6); 
        sum1 = sum(vector);
        sum1 = sqrt(sum1);
        for i=1:(N/2)

            sum2 = sum2 + vector(i);
            WKBD_N(i) = sqrt(sum2) / sum1;
            WKBD_N(2049-i) =  WKBD_N(i);

        end


        sum1 = 0;
        sum2 = 0;

        % WKBD with 256 points

        vector = kaiser(129,4); 
        sum1 = sum(vector);
        sum1 = sqrt(sum1);
        for i=1:128
            sum2 = sum2 + vector(i);
            WKBD_N8(i) = sqrt(sum2) / sum1;
            WKBD_N8(257-i) =  WKBD_N8(i);
        end
        
        y(1:N/2,1) = frameT(1:N/2,1) .* WKBD_N(1:N/2);
        y(1:N/2,2) = frameT(1:N/2,2) .* WKBD_N(1:N/2);
       
        y((N/2+1):(N/2+448),1) = frameT((N/2+1):(N/2+448),1);
        y((N/2+1):(N/2+448),2) = frameT((N/2+1):(N/2+448),2);
         
        y((N/2+448+1):(N/2+127+448+1),1) = frameT((N/2+448+1):(N/2+127+448+1),1) .* WKBD_N8(129:256);
        y((N/2+448+1):(N/2+127+448+1),2) = frameT((N/2+448+1):(N/2+127+448+1),2) .* WKBD_N8(129:256);
        
    elseif (strcmp(frameType,'LPS'))
       
        sum1 = 0;
        sum2 = 0;

        %% WKBD for OLS with 2048 points
        vector = kaiser(N/2+1,6); 
        sum1 = sum(vector);
        sum1 = sqrt(sum1);
        for i=1:(N/2)

            sum2 = sum2 + vector(i);
            WKBD_N(i) = sqrt(sum2) / sum1;
            WKBD_N(2049-i) =  WKBD_N(i);

        end


        %% WKBD with 256 points
        
        sum1=0;
        sum2=0;
        
        vector = kaiser(129,4); 
        sum1 = sum(vector);
        sum1 = sqrt(sum1);
        for i=1:128
            sum2 = sum2 + vector(i);
            WKBD_N8(i) = sqrt(sum2) / sum1;
            WKBD_N8(257-i) =  WKBD_N8(i);
        end
    
        y((449):(449+127),1) = frameT((449):(449+127),1) .* WKBD_N8(1:128) ;
        y((449):(449+127),2) = frameT((449):(449+127),2) .* WKBD_N8(1:128) ;
       
        y((449+128):(449+128+447),1) = frameT((449+128):(449+128+447),1) ;
        y((449+128):(449+128+447),2) = frameT((449+128):(449+128+447),2) ;
        
        y(N/2+1:N,1) = frameT(N/2+1:N,1) .* WKBD_N(N/2+1:N) ;
        y(N/2+1:N,2) = frameT(N/2+1:N,2) .* WKBD_N(N/2+1:N) ;
       
        
    else
        
        %% WKBD with 256 points
        sum1=0;
        sum2=0;
        
         
        vector = kaiser(129,4); 
        sum1 = sum(vector);
        sum1 = sqrt(sum1);
        for i=1:128
            sum2 = sum2 + vector(i);
            WKBD_N8(i) = sqrt(sum2) / sum1;
            WKBD_N8(257-i) =  WKBD_N8(i);
        end
    
        
        ESH_left = buffer([zeros(128,1) ; frameT(449:(449+1151),1) ; zeros(128,1)],256,128,'nodelay');
        ESH_right = buffer([zeros(128,1) ; frameT(449:(449+1151),2) ; zeros(128,1)],256,128,'nodelay');
        
        for i=1:10
            
            L_ESH(i,:) = ESH_left(:,i) .* WKBD_N8(1:256);
            R_ESH(i,:) = ESH_right(:,i) .* WKBD_N8(1:256);    
            
        end
        
       
    end
    
 else 
    
    %% Window Function: sinusoid (SIN)
    
    % Apply the Window
    if (strcmp(frameType,'OLS'))
        
        %% WSIN with 2056 points
        WSIN_N = (sin((pi/2048)*(n1+0.5)))';
   
        y(1:N,1) = frameT(1:N,1) .* WSIN_N(1:N);
        y(1:N,2) = frameT(1:N,2) .* WSIN_N(1:N);
        
    elseif (strcmp(frameType,'LSS'))
        
        %% WSIN with 2056 points
        WSIN_N = (sin((pi/2048)*(n1+0.5)))';
    
        %% WSIN with 256 points
        WSIN_N8 = (sin((pi/256)*(n2+0.5)))';
        
        y(1:N/2,1) = frameT(1:N/2,1) .* WSIN_N(1:N/2);
        y(1:N/2,2) = frameT(1:N/2,2) .* WSIN_N(1:N/2);
       
        y((N/2+1):(N/2+448),1) = frameT((N/2+1):(N/2+448),1);
        y((N/2+1):(N/2+448),2) = frameT((N/2+1):(N/2+448),2);
         
        y((N/2+448+1):(N/2+127+448+1),1) = frameT((N/2+448+1):(N/2+127+448+1),1) .* WSIN_N8(129:256);
        y((N/2+448+1):(N/2+127+448+1),2) = frameT((N/2+448+1):(N/2+127+448+1),2) .* WSIN_N8(129:256);
        
    elseif (strcmp(frameType,'LPS'))
        
        %% WSIN with 2056 points
        WSIN_N = (sin((pi/2048)*(n1+0.5)))';
    
        %% WSIN with 256 points
        WSIN_N8 = (sin((pi/256)*(n2+0.5)))';
    
        y((449):(449+127),1) = frameT((449):(449+127),1) .* WSIN_N8(1:128) ;
        y((449):(449+127),2) = frameT((449):(449+127),2) .* WSIN_N8(1:128) ;
       
        y((449+128):(449+128+447),1) = frameT((449+128):(449+128+447),1) ;
        y((449+128):(449+128+447),2) = frameT((449+128):(449+128+447),2) ;
        
        y(N/2+1:N,1) = frameT(N/2+1:N,1) .* WSIN_N(N/2+1:N) ;
        y(N/2+1:N,2) = frameT(N/2+1:N,2) .* WSIN_N(N/2+1:N) ;
        
    else
       
    
        %% WSIN with 256 points
        WSIN_N8 = (sin((pi/256)*(n2+0.5)))';
        
        ESH_left = buffer([zeros(128,1) ; frameT(449:(449+1151),1) ; zeros(128,1)],256,128,'nodelay');
        ESH_right = buffer([zeros(128,1) ; frameT(449:(449+1151),2) ; zeros(128,1)],256,128,'nodelay');
        
        for i=1:10
            
            L_ESH(i,:) = ESH_left(:,i) .* WSIN_N8(1:256);
            R_ESH(i,:) = ESH_right(:,i) .* WSIN_N8(1:256);    
            
        end
        
        
    end
    
    
    
end

%% Modified Discrete Cosine Transform (MDCT)

if (strcmp(frameType,'ESH'))
    
   %%%128x8x2 
   frameF = zeros(128,8,2);
   
   for (i=1:8)
       
       frameF(1:128,i,1) = mdct4(L_ESH(i+1,:));
       frameF(1:128,i,2) = mdct4(R_ESH(i+1,:));
      
   end
    
else 
    
    %%% 1024x2 
    frameF = zeros(N/2,2);
    
    frameF = mdct4(y);
    
end

end
