function x = iAACoder2(AACSeq2, fNameOut)
    
    [rows, columns] = size(AACSeq2);
    if(rows > columns)
        NumberOfFrames = rows;
    else
        NumberOfFrames = columns;
    end
    
    for i = 1:NumberOfFrames
        
        
        if ( strcmp( AACSeq2(i).frameType, 'ESH' ) )
            
%             AACSeq2(i).chl.frameF = AACSeq2(i).chl.frameF';
%             AACSeq2(i).chr.frameF = AACSeq2(i).chl.frameF';
            
%             size( AACSeq2(i).chl.frameF )
            
            AACSeq2(i).chl.frameF = iTNS(AACSeq2(i).chl.frameF, AACSeq2(i).frameType, AACSeq2(i).chl.TNScoeffs);
            AACSeq2(i).chr.frameF = iTNS(AACSeq2(i).chr.frameF, AACSeq2(i).frameType, AACSeq2(i).chr.TNScoeffs);
            
        else
            
            AACSeq2(i).chl.frameF = iTNS(AACSeq2(i).chl.frameF, AACSeq2(i).frameType, AACSeq2(i).chl.TNScoeffs);
            AACSeq2(i).chr.frameF = iTNS(AACSeq2(i).chr.frameF, AACSeq2(i).frameType, AACSeq2(i).chr.TNScoeffs);
            
        end
        
    end
    
    x = iAACoder1( AACSeq2, fNameOut );

    %audiowrite(fNameOut, x, 48000);

end