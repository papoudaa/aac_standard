function AACSeq2 = AACoder2(fNameIn)

AACSeq2 = AACoder1(fNameIn);

for i = 1:length(AACSeq2)
   
    [AACSeq2(i).chl.frameF,AACSeq2(i).chl.TNScoeffs] = TNS(AACSeq2(i).chl.frameF,AACSeq2(i).frameType);
    [AACSeq2(i).chr.frameF,AACSeq2(i).chr.TNScoeffs] = TNS(AACSeq2(i).chr.frameF,AACSeq2(i).frameType);
    
end
    

end